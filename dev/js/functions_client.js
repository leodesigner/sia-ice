//Nav_fixed
$(window).on('scroll',function() {
	var scrolltop = $(this).scrollTop();

	if(scrolltop >= 90) {
	  $('.nav-client').addClass('fixed-client');
	}

	else if(scrolltop <= 90) {
	  $('.nav-client').removeClass('fixed-client');
	}

	if(scrolltop >= 1200) {
    	$(".animated").addClass("fadeInUp");
    }	

});

$('.collapse a').on('click', function(){
    $('.navbar-toggler').click();
});

$('.single-item').slick({
    arrows: true,
    infinite: true,
    slidesToShow: 1,
    rows: 1,
    swipeToSlide: true,
    //asNavFor: '.sabores',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                arrows: false,
                dots: true,
            }
        },
        {
            breakpoint: 600,
            settings: {
                arrows: false,
                dots: true,
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                dots: true,
            }
        }
    ]
});

//Galeria Tsunami
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
//smoothscroll
// var scroll = new SmoothScroll('a.scroll_tsu',{
// 	header: '#top_page',
// 	speed: 2500,
//     scrollTarget: this.hash,
// 	offset: 60
// });

$('a.scroll_tsu').on('click',function (e) {
    // e.preventDefault();

    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top-90
    }, 2500, 'swing', function () {
     window.location.hash = target;
    });
});